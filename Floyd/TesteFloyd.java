//import floyd.*;
import java.util.Scanner;
public class TesteFloyd {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
    String comando = "";
    int valorGlobal = 0;

    while(comando != "S"){
        int valor = input.nextInt();
        comando = "S";
        valorGlobal = valor;
    }

		Floyd floyd = new Floyd(valorGlobal);
		System.out.println("-");

		while(input.hasNext()){
      comando = input.next();
      if (comando.equals("edge")){
        int valor1 = input.nextInt();
        int valor2 = input.nextInt();
        int valor3 = input.nextInt();
        floyd.adicionarAresta(valor1,valor2,valor3);
        floyd.adicionarAresta(valor2,valor1,valor3);
        System.out.println("-");
      }
      else{
      	floyd.floydAr();
      	System.out.println();
      	floyd.printMatriz( floyd.matrizArestas );
      	System.out.println();
      	floyd.printMatriz( floyd.matrizPredecessor );
      	System.out.println();


      	if (comando.equals("shortest")) {
      		int valor1 = input.nextInt();
        	int valor2 = input.nextInt();
        	System.out.println( floyd.shortest(valor1, valor2) );
      	}
      }
    }






		/*//grafo do slide
		floyd.adicionarAresta(0,1,8);
		floyd.adicionarAresta(0,3,3);
		floyd.adicionarAresta(0,4,3);

		floyd.adicionarAresta(1,0,8);
		floyd.adicionarAresta(1,2,5);
		floyd.adicionarAresta(1,4,2);

		floyd.adicionarAresta(2,1,5);
		floyd.adicionarAresta(2,4,5);
		floyd.adicionarAresta(2,5,3);

		floyd.adicionarAresta(3,0,3);
		floyd.adicionarAresta(3,4,2);

		floyd.adicionarAresta(4,0,3);
		floyd.adicionarAresta(4,1,2);
		floyd.adicionarAresta(4,2,5);
		floyd.adicionarAresta(4,3,2);
		floyd.adicionarAresta(4,5,4);
		//\grafo do slide

		floyd.printMatriz();*/
	}
}