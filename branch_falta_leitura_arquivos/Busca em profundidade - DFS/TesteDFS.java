import representacoes.*; //MatrizAdjacencia.java e ListaAdjacencia.java
import java.util.Scanner;
public class TesteDFS{
    public static void main(String[] args){
	Scanner input = new Scanner(System.in);
	String comando = "";
	int valorGlobal = 0;

	while(comando != "S"){
	    int valor = input.nextInt();
	    comando = "S";
	    valorGlobal = valor;
	}

	MatrizAdjacencia obj = new MatrizAdjacencia();
	DFS obj1 = new DFS();
	obj.iniciaMatrizDeAdjacencia(valorGlobal);
	System.out.println("-");

	while(comando != "P"){
	    comando = input.next();
	    if (comando.equals("edge")){
		int valor1 = input.nextInt();
		int valor2 = input.nextInt();
	        obj.iniciaArestasDaMatriz(valor1,valor2);
		obj.iniciaArestasDaMatriz(valor2,valor1);
		System.out.println("-");
	    }
	    else{
		if ((comando.equals("shortest")) ||
		    (comando.equals("path"))){
		    int valor1 = input.nextInt();
		    int valor2 = input.nextInt();
		    if (obj1.Dfs(valor1, valor2, obj) != true){
			System.out.println("No path");
		    }
		    else{
			obj1.Dfs(valor1, valor2, obj);
			obj1.imprimirCaminho();
		    }
		}
		else comando = "P";
	    }
	}

	// MatrizAdjacencia obj = new MatrizAdjacencia();
	// DFS obj1 = new DFS();
	// obj.iniciaMatrizDeAdjacencia(5);
	// // obj.iniciaArestasDaMatriz(0, 1);
	// // obj.iniciaArestasDaMatriz(1, 2);
	// // obj.iniciaArestasDaMatriz(2, 3);
	// // obj.iniciaArestasDaMatriz(3, 4);
	// // obj.iniciaArestasDaMatriz(4, 3);
	// // obj.iniciaArestasDaMatriz(3, 2);
	// // obj.iniciaArestasDaMatriz(2, 1);
	// // obj.iniciaArestasDaMatriz(1, 0);
	
	// // obj.iniciaArestasDaMatriz(0, 1);
	// // obj.iniciaArestasDaMatriz(1, 0);
	// // obj.iniciaArestasDaMatriz(2, 1);
	// // obj.iniciaArestasDaMatriz(1, 2);
	// // obj.iniciaArestasDaMatriz(2, 3);
	// // obj.iniciaArestasDaMatriz(3, 2);
	// // obj.iniciaArestasDaMatriz(3, 4);
	// // obj.iniciaArestasDaMatriz(4, 3);

	// obj.iniciaArestasDaMatriz(0, 2);
	// obj.iniciaArestasDaMatriz(2, 4);
	// obj.iniciaArestasDaMatriz(4, 3);
	// obj.iniciaArestasDaMatriz(3, 1);
	// obj.iniciaArestasDaMatriz(2, 0);
	// obj.iniciaArestasDaMatriz(4, 2);
	// obj.iniciaArestasDaMatriz(3, 4);
	// obj.iniciaArestasDaMatriz(1, 3);

	// obj.imprimirMatrizDeAdjacencia();
	// obj1.Dfs(0,1,obj);
    }
}
