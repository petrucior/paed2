import representacoes.*; //MatrizAdjacencia.java e ListaAdjacencia.java
import conjuntosDisjuntos.*; //ConjuntosDisjuntos.java
import java.util.Scanner;
public class TestePrim{
    public static void main(String[] args){
	
	Scanner input = new Scanner(System.in);
	String comando = "";
	int valorGlobal = 0;

	while(comando != "S"){
	    int valor = input.nextInt();
	    comando = "S";
	    valorGlobal = valor;
	}

        ListaAdjacencia obj = new ListaAdjacencia();
	obj.iniciaListaDeAdjacencia(valorGlobal);
	Prim obj1 = new Prim();
	System.out.println("-");

	while(input.hasNext()){
	// while(comando != "P"){
	    comando = input.next();
	    if (comando.equals("edge")){
		int valor1 = input.nextInt();
		int valor2 = input.nextInt();
		int valor3 = input.nextInt();
	        obj.iniciaArestasDaListaComPeso(valor1,valor2,valor3);
		obj.iniciaArestasDaListaComPeso(valor2,valor1,valor3);
		System.out.println("-");
	    }
	    else{
		if (comando.equals("prim")){
		    obj1.prim(obj);
		}
		// else comando = "P";
	    }
	}
	
	// ListaAdjacencia obj = new ListaAdjacencia();
	// // obj.iniciaListaDeAdjacencia(5);
	// // //Adicionando arestas com custo e com orientacao na lista
	// // obj.iniciaArestasDaListaComPeso(0, 2, 4);
	// // obj.iniciaArestasDaListaComPeso(2, 0, 4);
	// // obj.iniciaArestasDaListaComPeso(1, 4, 10);
	// // obj.iniciaArestasDaListaComPeso(4, 1, 10);
	// // obj.iniciaArestasDaListaComPeso(4, 3, 3);
	// // obj.iniciaArestasDaListaComPeso(3, 4, 3);
	// // obj.iniciaArestasDaListaComPeso(4, 2, 4);
	// // obj.iniciaArestasDaListaComPeso(2, 4, 4);
	// // obj.iniciaArestasDaListaComPeso(3, 2, 5);
	// // obj.iniciaArestasDaListaComPeso(2, 3, 5);
	
	
	// obj.iniciaListaDeAdjacencia(4);
	// obj.iniciaArestasDaListaComPeso(0, 1, 5);
	// obj.iniciaArestasDaListaComPeso(1, 0, 5);
	// obj.iniciaArestasDaListaComPeso(1, 2, 2);
	// obj.iniciaArestasDaListaComPeso(2, 1, 2);
	// obj.iniciaArestasDaListaComPeso(0, 2, 1);
	// obj.iniciaArestasDaListaComPeso(2, 0, 1);
	// obj.iniciaArestasDaListaComPeso(1, 3, 3);
	// obj.iniciaArestasDaListaComPeso(3, 1, 3);
	// obj.iniciaArestasDaListaComPeso(3, 2, 1);
	// obj.iniciaArestasDaListaComPeso(2, 3, 1);
	// obj.imprimirListaDeAdjacencia();
	// Prim obj1 = new Prim();
	// obj1.prim(obj);
    }
}
