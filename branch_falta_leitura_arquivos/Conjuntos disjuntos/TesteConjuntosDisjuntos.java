import representacoes.*; //MatrizAdjacencia.java e ListaAdjacencia.java
import conjuntosDisjuntos.*; //ConjuntosDisjuntos.java 
import java.util.Scanner;
public class TesteConjuntosDisjuntos{
    public static void main(String[] args){
	Scanner input = new Scanner(System.in);
	String comando = "";
	int valorGlobal = 0;

	while(comando != "S"){
	    int valor = input.nextInt();
	    comando = "S";
	    valorGlobal = valor;
	}

	ListaAdjacencia obj = new ListaAdjacencia();
	obj.iniciaListaDeAdjacencia(valorGlobal);
	ConjuntosDisjuntos obj1 = new ConjuntosDisjuntos(obj);
	System.out.println("-");

	while(input.hasNext()){
	// while(comando != "P"){
	    comando = input.next();
	    if (comando.equals("compare")){
		int valor1 = input.nextInt();
		int valor2 = input.nextInt();
		if (obj1.compare(valor1, valor2) == true)
		    System.out.println("true");
		else System.out.println("false");
	    }
	    else{
		if (comando.equals("union")){
		    int valor1 = input.nextInt();
		    int valor2 = input.nextInt();
		    obj1.union(valor1, valor2, obj);
		    System.out.println("-");
		}
		// else comando = "P";
	    }
	}

    	// ListaAdjacencia obj = new ListaAdjacencia();
	// obj.iniciaListaDeAdjacencia(5);
	// ConjuntosDisjuntos obj1 = new ConjuntosDisjuntos(obj);
	// obj1.imprimirRepresentantes();
	// obj1.imprimirOrdens();
	// System.out.println("-----------");
	// obj1.union(0,1,obj);
	// obj1.imprimirRepresentantes();
	// obj1.imprimirOrdens();
	// System.out.println("-----------");
	// obj1.union(0,2,obj);
	// obj1.imprimirRepresentantes();
	// obj1.imprimirOrdens();
	// System.out.println("-----------");
	// obj1.union(3,4,obj);
	// obj1.imprimirRepresentantes();
	// obj1.imprimirOrdens();
	// System.out.println("-----------");
	// if (obj1.compare(0,2) == true){
	//     System.out.println("Estao no mesmo conjunto");
	// }
	// else System.out.println("Nao estao no mesmo conjunto");
	// obj1.imprimirRepresentantes();
	// obj1.imprimirOrdens();
	// System.out.println("-----------");
    }
}
