import representacoes.*; //MatrizAdjacencia.java e ListaAdjacencia.java
public class Dijkstra{
    int dist[]; //Distancia dos vertices
    int pred[]; //Predecessor dos vertices
    boolean vis[]; //Vetor de visitados
    int path[];
    int tam;
    int cont = 0;
    //atualiza os vetores
    public void atualizaVetores(ListaAdjacencia L){
	//Tamanho da lista de adjacencia
	tam = L.verificarTamLista();
	//Instanciando vetores
	dist = new int[tam];
	pred = new int[tam];
	vis = new boolean[tam];
	path = new int[tam];
	//laco para atualizar os vetores
	for (int i = 0; i < tam; i++){
	    dist[i] = 1000;
	    pred[i] = -1;
	    vis[i] = false;
	    path[i] = -1;
	}
    }

    //relaxa as arestas
    public void relaxarArestas(int verticeInicial, 
			       int verticeFinal,
			       int custoAresta){
	//Se a distancia do verticeInicial + custoAresta < dist verticeFinal
	//entao temos que atualizar o vetor dist e pred
	if (dist[verticeInicial] + custoAresta < dist[verticeFinal]){
	    dist[verticeFinal] = dist[verticeInicial] + custoAresta;
	    pred[verticeFinal] = verticeInicial;
	}
    }

    public void iniciarVertices(int vertice){
	dist[vertice] = 0;
	pred[vertice] = vertice;
    }

    //metodo de shortest - Encontrar o menor caminho
    public void shortest(ListaAdjacencia L,
			 int verticeInicial,
			 int verticeFinal){
	cont = 0;
	for (int i = 0; i < L.L[verticeInicial].getTam(); i++){
	    //Se nao visitou o conteudo da posicao i da lista do verticeInicial
	    if (vis[L.L[verticeInicial].get(i).getInfo()] == false){
		relaxarArestas(verticeInicial,
			       L.L[verticeInicial].get(i).getInfo(),
			       L.L[verticeInicial].get(i).getCust());
	    }
	}
	
	vis[verticeInicial] = true;

	int menor = 1000;
	int indice = 1000;
	for (int i = 0; i < dist.length; i++){
	    //Verificando quem � a menor distancia
	    if ((vis[i] == false) && (dist[i] < menor)){
		menor = dist[i];
		indice = i;
	    }
	}
	// imprimirDistancia();
	if (menor == 1000) return;
	else
	    shortest(L, indice, verticeFinal);
    }
    
    public void imprimirDistancia(){
	for (int i = 0; i < dist.length; i++){
	    System.out.printf("%d ", dist[i]);
	}
	System.out.println();
    }

    public void imprimirPrecedencia(){
	for (int i = 0; i < dist.length; i++){
	    System.out.printf("%d ", pred[i]);
	}
	System.out.println();
    }

    public void imprime(){
        int c = 0;
	for (int i = path.length - 1; i > -1; i--){
	    if (path[i] != -1){
		c = path[i];
		System.out.printf("%d ", path[i]);
	    }
	}
	System.out.printf("%d", dist[c]);
	System.out.println();
    }


    public void caminho(int verticeInicial, int verticeFinal){
	if ((pred[verticeFinal] == -1) || (pred[verticeInicial] == -1)){
	    System.out.println("No path");
	    return;
	}
	else{
	    if (pred[verticeFinal] != verticeInicial){
		path[cont] = verticeFinal;
		cont++;
		caminho(verticeInicial, pred[verticeFinal]);
	    }
	    else{
		path[cont] = verticeFinal;
		path[cont+1] = verticeInicial;
		imprime();
		return;
	    }
	}
    }
    
}
