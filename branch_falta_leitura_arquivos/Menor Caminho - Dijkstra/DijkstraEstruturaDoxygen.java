/**
 * \file Dijkstra.java
 *
 * \brief Definicao da classe de menor caminho utilizando dijkstra.
 *
 * \author Petrucio Ricardo Tavares de Medeiros
 *
 * \version 1.0
 */
import representacoes.*; //MatrizAdjacencia.java e ListaAdjacencia.java
/**
 * \class Dijkstra
 *
 * \brief A classe do algoritmo dijkstra.
 */
public class DijkstraTestandoClasse{

    //--------------------------------------------------------------------
    //                       Atributo Privado
    //--------------------------------------------------------------------
    int dist[]; //Distancia dos vertices
    int pred[]; //Predecessor dos vertices
    boolean vis[]; //Vetor de visitados
    int cam[]; //Vetor de caminhos
    int tam;
    
    //--------------------------------------------------------------------
    //                       Metodo Privado
    //--------------------------------------------------------------------
    //Atualiza os vetores dist, pred e vis
    public void atualizaVetores(ListaAdjacencia L){
	//Tamanho da lista de adjacencia
	tam = L.verificarTamLista();
	//Instanciando vetores
	dist = new int[tam];
	pred = new int[tam];
	vis = new boolean[tam];
	//laco para atualizar os vetores
	for (int i = 0; i < tam; i++){
	    dist[i] = 1000;
	    pred[i] = -1;
	    vis[i] = false;
	}
	dist[0] = 0;
	pred[0] = 0;
    }

    //Relaxar arestas
    public void relaxarArestas(int verticeInicial, 
			   int verticeFinal,
			   int custoAresta){
	//Se a distancia do verticeInicial + custoAresta < dist verticeFinal
	//entao temos que atualizar o vetor dist e pred
	if (dist[verticeInicial] + custoAresta < dist[verticeFinal]){
	    dist[verticeFinal] = dist[verticeInicial] + custoAresta;
	    pred[verticeFinal] = verticeInicial;
	}
    }

    //Verifica se ainda falta percorrer vizinhos
    //Retorna true se ha elemento vizinho ainda nao percorrido e false c.c.
    public boolean haElementosVizinhosNaoPercorridos(){
	for (int i = 0; i < tam; i++){
	    if (vis[i] == false) return true;
	}
	return false;
    }

    //Verifica a maior distancia no vetor de distancia
    public int maiorDistancia(){
	int maior = 0;
	for (int i = 0; i < tam; i++){
	    if (dist[i] > maior){
		maior = dist[i];
	    }
	}
	return maior;
    }

    public boolean relaxouAlgumaAresta(int verticeInicial){
	int cont = 0;
	for (int i = 0; i < tam; i++){
	    if (dist[i] == 1000){
		cont++;
	    }
	}
	System.out.printf("o tam - vI:%d e cont: %d", tam-verticeInicial - 1, cont);
	System.out.println();
	if ((tam - verticeInicial - 1) == cont){
	    return false;
	}
	return true;
    }

    //Verifica a menor distancia ainda nao percorrida
    //Retorna o indice do vertice com menor distancia
    public int menorDistanciaNaoPercorrida(int vertice){
	int menor = maiorDistancia() + 1;
	int indice = 0;
	for (int i = 0; i < tam; i++){
	    if ((dist[i] < menor) && (dist[i] >= dist[vertice]) &&
		(i != vertice) && (vis[i] == false)){
		menor = dist[i];
		indice = i;
	    }
	}
	return indice;
    }

    //Atualizando as distancias de vizinhos de um determinado vertice
    public void atualizarVerticesVizinhos(ListaAdjacencia L,
					   int verticeInicial){
	for (int i = 0; i < (L.L[verticeInicial]).getTam(); i++){
	    if (vis[(L.L[verticeInicial].get(i)).getInfo()] == false){
		relaxarArestas(verticeInicial,
			       L.L[verticeInicial].get(i).getInfo(),
			       L.L[verticeInicial].get(i).getCust());
	    }
	}
    }
    
    //Percorrendo Lista de Adjacencia
    public void percorrerListaAdjacencia(ListaAdjacencia L, 
					   int verticeInicial){
	if (haElementosVizinhosNaoPercorridos()){
	    atualizarVerticesVizinhos(L, verticeInicial);
	    int menor = menorDistanciaNaoPercorrida(verticeInicial);
	    vis[verticeInicial] = true;
	    if (!relaxouAlgumaAresta(verticeInicial)){
		dist[verticeInicial] = 1000;
		pred[menor] = verticeInicial;
		dist[menor] = 0;
		percorrerListaAdjacencia(L, menor);
	    }
	    else percorrerListaAdjacencia(L, menor);
	}
	else return;
    }

    //Imprimir distancia dos vertices
    public void imprimirDistancias(){
	for (int i = 0; i < dist.length; i++){
	    System.out.printf("%d %d %d \n",i,dist[i], pred[i]);
	}
    }
	
    //Metodo de Dijkstra
    public void dijkstra(ListaAdjacencia L){
	//Inicializa vetores dist, pred e 
	atualizaVetores(L);
	//Realizando o percorrimento dos vertices e toda a l�gica
	percorrerListaAdjacencia(L, 0);
	//Imprimindo as distancias
	imprimirDistancias();
    }

    public void shortest(int verticeInicio, int verticeFim){
        if (pred[verticeFim] != -1){
    	    int contador = 0;
    	    int i = pred[verticeFim];
    	    while(i != verticeInicio){
    		i = pred[i];
    		if (contador > tam + 2) return;
    		else contador++;
    	    }
    	    if (contador < tam + 2){
    		cam = new int[contador+1];
    		i = pred[verticeFim];
    		while(i != verticeInicio){
    		    // System.out.printf("%d", i);
    		    cam[contador--] = i;
    		    i = pred[i];
    		}
    		for (int j = 0; j < cam.length; j++){
    		    System.out.printf("%d ", cam[j]);
    		}
    		return;
    	    }
	    
    	}
    	else return;
    }
}

