import representacoes.*; //MatrizAdjacencia.java e ListaAdjacencia.java
import java.util.Scanner;
public class TesteDijkstra{
    public static void main(String[] args){
	
	Scanner input = new Scanner(System.in);
	String comando = "";
	int valorGlobal = 0;

	while(comando != "S"){
	    int valor = input.nextInt();
	    comando = "S";
	    valorGlobal = valor;
	}

        ListaAdjacencia obj = new ListaAdjacencia();
	obj.iniciaListaDeAdjacencia(valorGlobal);
	Dijkstra obj1 = new Dijkstra();
	System.out.println("-");

	while(input.hasNext()){
	// while(comando != "P"){
	    comando = input.next();
	    if (comando.equals("edge")){
		int valor1 = input.nextInt();
		int valor2 = input.nextInt();
		int valor3 = input.nextInt();
	        obj.iniciaArestasDaListaComPeso(valor1,valor2,valor3);
		System.out.println("-");
	    }
	    else{
		if (comando.equals("shortest")){
		    obj1.atualizaVetores(obj);
		    int valor1 = input.nextInt();
		    int valor2 = input.nextInt();
		    obj1.iniciarVertices(valor1);
		    obj1.shortest(obj, valor1, valor2);
		    // obj1.imprimirPrecedencia();
		    obj1.caminho(valor1, valor2);
		}
		// else comando = "P";
	    }
	}

	// ListaAdjacencia obj = new ListaAdjacencia();
	// obj.iniciaListaDeAdjacencia(6);
	// //Adicionando arestas com custo e com orientacao na lista
	// obj.iniciaArestasDaListaComPeso(1, 2, 1);
	// obj.iniciaArestasDaListaComPeso(2, 3, 4);
	// obj.iniciaArestasDaListaComPeso(3, 5, 6);
	// obj.iniciaArestasDaListaComPeso(2, 4, 2);
	// obj.iniciaArestasDaListaComPeso(0, 1, 3);
	// obj.iniciaArestasDaListaComPeso(0, 5, 2);
	// obj.imprimirListaDeAdjacencia();
	// Dijkstra obj1 = new Dijkstra();
	// // obj1.atualizaVetores(obj);
	// // obj1.atualizarVerticesVizinhos(obj, 0);
	// // System.out.printf("mDist:%d\n",obj1.menorDistanciaNaoPercorrida(0));
	// // obj1.vis[0] = true;
	// // obj1.imprimirDistancias();
	// obj1.dijkstra(obj);
	// obj1.shortest(1, 3);
    }
}
